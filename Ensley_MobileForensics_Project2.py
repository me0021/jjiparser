import sys

global HEADER
global FOOTER
global counter
global isProcessing
global chunkToWrite

HEADER = bytearray(b'\x00J\x00O\x00S\x00H')
FOOTER = bytearray(b'J\x00O\x00N\x00E\x00S')
counter = 0
isProcessing = 0
chunkToWrite = bytearray(b'')

def chunkOut(line):
    global counter
    global isProcessing
    global chunkToWrite
    if (HEADER not in line and FOOTER not in line):
        chunkToWrite.extend(line)
    elif FOOTER in line:
        line = line.split(FOOTER)
        chunkToWrite.extend(line[0])
        chunkToWrite.extend(FOOTER)
        outFile = open('file' + str(counter) + ".jji", 'wb')
        outFile.write(chunkToWrite)
        outFile.close()
        counter += 1
        isProcessing = 0
        chunkToWrite = bytearray(b'')
        del outFile
    elif HEADER in line:
        line = line.split(HEADER)
        chunkToWrite.extend(HEADER)
        chunkToWrite.extend(line[1])
    return

def processFile():
    global isProcessing
    jji = open('jji_project.001', 'rb')
    for line in jji.readlines():
        if isProcessing:
            chunkOut(line)
        if HEADER in line:
            isProcessing = 1
            chunkOut(line)
    return

if __name__ == "__main__":
    processFile()
    sys.exit(0)